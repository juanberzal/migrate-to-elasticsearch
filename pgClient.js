'use strict'

const {
	Client
} = require('pg')

const sequelizeDBConnectionUri = ""

const client = new Client({
	connectionString: sequelizeDBConnectionUri, // TODO read connection
})

module.exports = {
	client,
	* connect() {
		return new Promise(function (resolve, reject) {
			// TODO: There sould be just one way to get a DB connection
			client.connect(err => {
				if (err) reject(err)
				else resolve()
			})
		})
	},
}
