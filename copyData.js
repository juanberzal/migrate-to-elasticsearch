'use strict'

const fs = require('fs')
const co = require('co')
const copyTo = require('pg-copy-streams').to
const pgClient = require('./pgClient')

co(function* () {
	yield pgClient.connect()
	const client = pgClient.client
	const generateCSV = new Promise(function (resolve, reject) {
		var stream = client.query(copyTo(`COPY participations TO STDOUT WITH DELIMITER ';' CSV HEADER`))
		var fileStream = fs.createWriteStream('./table_participations.csv')
		stream.pipe(fileStream)
			.on('end', resolve)
			.on('error', reject)
	})
	yield Promise.all([generateCSV])
}).catch(err => {
	console.log(err)
})
